const ingAtmService = require("../services/ingAtm.service")

module.exports.getAtmAvilable = async(req, res) => {
    try {
        const result = await ingAtmService.getAtmAvilable(req.query)
        res.send(result)
    } catch (err) {
        console.error(err)
        return { error: err.message || err }
    }
}


module.exports.createINGAtm = async(req, res) => {
    try {
        const result = await ingAtmService.createINGAtm(req.body)
        res.send(result)
    } catch (err) {
        console.error(err)
        return res.send({ error: err.message || err })
    }
}

module.exports.getINGAtm = async(req, res) => {
    try {
        const result = await ingAtmService.getINGAtm(req.query)
        res.send(result)
    } catch (err) {
        console.error(err)
        return res.send({ error: err.message || err })
    }
}

module.exports.deleteINGAtm = async(req, res) => {
    try {
        const result = await ingAtmService.deleteINGAtm(req.params)
        res.send(result)
    } catch (err) {
        console.error(err)

        return res.send({ error: err.message || err })
    }
}

module.exports.updateINGAtm = async(req, res) => {
    try {
        const result = await ingAtmService.updateINGAtm(req.body)
        res.send(result)
    } catch (err) {
        console.error(err)
        return res.send({ error: err.message || err })
    }
}