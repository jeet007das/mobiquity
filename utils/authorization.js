let { user_collection = [] } = require("../datasource/index")
const bcrypt = require("bcrypt-nodejs")


module.exports.gurdian = async(req, res, next) => {
    try {
        if (!req.headers.authorization || req.headers.authorization.indexOf('Basic ') === -1) {
            return res.status(401).json({ message: 'Missing Authorization Header' });
        }

        // verify auth credentials
        const base64Credentials = req.headers.authorization.split(' ')[1];
        const credentials = Buffer.from(base64Credentials, 'base64').toString('ascii');
        const [username, password] = credentials.split(':');
        // console.log("============== USER NAME ==========", username, password, user_collection);
        if (user_collection.length) {
            const userInfo = user_collection.find(user => user.userID === username)
            if (userInfo) {
                const isValid = await bcrypt.compareSync(password, userInfo.password)
                if (isValid) {
                    next()
                } else {
                    return res.status(401).json({ message: 'Incorrect password' });
                }
            } else {
                return res.status(401).json({ message: 'User not found' });
            }
        } else {
            return res.status(401).json({ message: "User Collection not found." })
        }

    } catch (e) {
        console.log(e)
        return res.send({ code: 0, message: e.message });
    }
}