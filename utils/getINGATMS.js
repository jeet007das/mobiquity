const requests = require('requests');
const { iNG_AtmDatasetUrl = "" } = require("../config/index")
const { Chunk_Size = 50 } = require("../config/index");
const { ingAtm_dataset } = require("../datasource/index")

function dataProcessing(payload = []) {
    try {
        if (payload.length) {
            // const city_list = {}
            for (let data of payload) {
                const { city = "" } = data['address']
                data['isAvailable'] = true
                const first_letter = city && city.slice(0, 1).toUpperCase()
                if (ingAtm_dataset[first_letter]) {
                    ingAtm_dataset[first_letter].push(data)
                } else {
                    ingAtm_dataset[first_letter] = []
                    ingAtm_dataset[first_letter].push(data)
                }
            }
            return true
        }
    } catch (e) {
        console.error(e);
        return false
    }
}

function dataCleaning(rawData) {
    return new Promise(async(resolve, reject) => {
        try {
            //Removing bad value of Data file to parse.
            let dataSet = rawData.slice(5)
            dataSet = JSON.parse(dataSet)
            const main_data = []
            if (dataSet && dataSet.length) {
                const bucket_data = Math.round(dataSet.length / Chunk_Size)

                let i = 1;
                let set = 0
                do {
                    let chunk_data = dataSet.slice(set, bucket_data * i)
                    set = bucket_data * i
                    main_data.push(chunk_data)
                    i++;
                }
                while (i <= Chunk_Size);

                await Promise.all(main_data.length && main_data.map((data) => dataProcessing(data)))
                resolve(true)

            }
        } catch (e) {
            console.error(e);
            //  return { code: 0, message: e.message || e }
            reject()
        }
    })


}

function getINGAtmList() {
    return new Promise(async(resolve, reject) => {
        try {
            requests(iNG_AtmDatasetUrl, { streaming: true })
                .on('data', async(chunkData) => {
                    //Here in this function removing bad format of data due to parse the rawdata.
                    const res = await dataCleaning(chunkData)
                    if (res) {
                        console.log("=========== Data Cleaning and Data Processing Is successfully done ========")
                        resolve(true)
                    }


                })
                .on('end', function(err) {
                    if (err) {
                        console.error(err)
                        reject()
                    }
                });
        } catch (e) {
            console.error(e);
            reject(e)
        }
    })

}

module.exports.getINGAtmList = getINGAtmList