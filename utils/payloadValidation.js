module.exports.dataValidation = (payload = {}, fields = []) => {
    if (fields && fields.length) {
        const err_fields = []
        fields.forEach(field => {
            if (!payload[field]) {
                err_fields.push(field)
            }
        })

        if (!err_fields.length) return { code: 1, message: "Data validate successfully." }
        else return { code: 0, info: err_fields }
    }
}