/* eslint-disable new-cap */
const { Router } = require('express');
//import { validator } from '../validations/validator';
const ingAtmController = require('../controllers/ingAtm.controller')
const { gurdian } = require("../utils/authorization")
    //import { login } from '../validations/admin.validator';
const router = Router()
router.get('/getAtmsAvailable', gurdian, ingAtmController.getAtmAvilable);
router.post('/createIngAtm', gurdian, ingAtmController.createINGAtm);
router.get('/getIngAtm', gurdian, ingAtmController.getINGAtm);
router.delete('/:atmID', gurdian, ingAtmController.deleteINGAtm);
router.put('/updateIngAtm', gurdian, ingAtmController.updateINGAtm);


module.exports = router;