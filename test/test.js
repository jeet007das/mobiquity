const server = require('../index'),
    chai = require('chai'),
    chaiHttp = require('chai-http'),
    expect = chai.expect //to solve error when using done(): “ReferenceError: expect is not defined”
;

const { createUser } = require("../controllers/user.controller")
const { User_Data } = require("../config/index")
chai.use(chaiHttp);

//chai.use(chaiSubset);
describe('Testing ING ATM Services', function() {
    before(function(done) {
        createUser(User_Data).then(res => {
            if (res && res['code']) {
                done()
            }
        })
    });
    //When done is passed in, Mocha will wait until the call to done(), or until the timeout expires. done also accepts an error parameter when signaling completion.
    it('should read the available atm list in dutch city', function(done) { // <= Pass in done callback
        chai.request(server)
            .get('/api/ingAtm/getAtmsAvailable?cityName=Stadskanaal')
            .auth("Biswa", "Biswa@123")
            .end(function(err, res) {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object')
                expect(res.body).to.have.a.property('code');
                expect(res.body).to.include({ code: 1 });
                done(); // <= Call done to signal callback end
            });
    });
    it('should read the created ing atm list', function(done) { // <= Pass in done callback
        chai.request(server)
            .get('/api/ingAtm/getIngAtm')
            .auth("Biswa", "Biswa@123")
            .end(function(err, res) {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object')
                expect(res.body).to.have.a.property('code');
                expect(res.body).to.include({ code: 1 });
                done(); // <= Call done to signal callback end
            });
    });
    // it('should delete ing atm data', function(done) { // <= Pass in done callback

    // });

    it('should create new ing atm in dutch city location', function(done) { // <= Pass in done callback
        chai.request(server)
            .post('/api/ingAtm/createIngAtm')
            .auth("Biswa", "Biswa@123")
            .send({
                "address": {
                    "street": "Gulden Eind",
                    "housenumber": "9",
                    "postalcode": "6096 AG",
                    "city": "Grathem",
                    "geoLocation": {
                        "lat": "51.192415",
                        "lng": "5.860898"
                    }
                }
            })
            .end(function(err, res) {
                const { data = {} } = res.body

                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object')
                expect(res.body).to.have.a.property('code');
                expect(res.body).to.include({ code: 1 });
                console.log("<=========== Create test case passed successfully ========>")

                chai.request(server)
                    .put('/api/ingAtm/updateIngAtm')
                    .auth("Biswa", "Biswa@123")
                    .send(data)
                    .end(function(err, res) {
                        expect(res).to.have.status(200);
                        expect(res.body).to.be.an('object')
                        expect(res.body).to.have.a.property('code');
                        expect(res.body).to.include({ code: 1 });
                        console.log("<=========== Update test case passed successfully ========>")
                        chai.request(server)
                            .delete(`/api/ingAtm/${data.atmID}`)
                            .auth("Biswa", "Biswa@123")
                            .end(function(err, res) {
                                expect(res).to.have.status(200);
                                expect(res.body).to.be.an('object')
                                expect(res.body).to.have.a.property('code');
                                expect(res.body).to.include({ code: 1 });
                                console.log("<=========== Delete test case passed successfully ========>")

                                done(); // <= Call done to signal callback end
                            });

                    });
            });
    });





});