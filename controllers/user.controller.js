const userService = require("../services/user.service.js")

module.exports.createUser = async(payload = {}) => {
    try {
        const res = await userService.createUser(payload)
        return { code: 1, message: "User created successfully." }
    } catch (err) {
        console.error(err)
        return { error: err.message || err }
    }
}