const express = require('express');
const app = express();
const cors = require('cors');
var bodyParser = require('body-parser');
//const routes = require('./routes/index');
const ingAtmRouter = require('./routes/ingAtm.route');
const { PORT, User_Data } = require("./config/index")
const { createUser } = require("./controllers/user.controller")
const dataService = require("./utils/getINGATMS");
const { ingAtm_dataset } = require("./datasource/index")
const fs = require('fs');

//parse data

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

// allow cross orgin request
app.use(cors());
//connecting mongodb connecting
(async function() {
    try {
        await createUser(User_Data);
        fs.readFile('ingAtm_dataset.json', 'utf8', async function(err, data) {
            if (err) {
                console.error(err)
            }
            if (!data) {
                const res = await dataService.getINGAtmList()
                if (res) {
                    fs.writeFile('ingAtm_dataset.json', JSON.stringify(ingAtm_dataset), function(err) {
                        if (err) throw err;
                    })
                }
            }
        });

    } catch (error) {
        console.error(error);
        process.exit(1);
    }
})();




// //bind jwt token
// app.use(jwt())

//intializing routes
app.use('/api/ingAtm', ingAtmRouter);


// start server
const port = process.env.NODE_ENV === 'production' ? (process.env.PORT || 80) : PORT;
const server = app.listen(port, async() => {
    console.log('Server listening on port ' + port);
});

module.exports = server;