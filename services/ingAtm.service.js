//const { ingAtm_dataset = {} } = require("../datasource/index")
const fs = require("fs");

function readDataFile(jsonfileName) {
    return new Promise((resolve, reject) => {
        try {
            fs.readFile(jsonfileName, "utf8", async function(err, data) {
                if (err) {
                    reject({ code: 0, message: err.message });
                }
                if (data) {
                    resolve({ code: 1, data: data ? JSON.parse(data) : null });
                } else {
                    resolve({ code: 1, data: [] });
                }
            });
        } catch (e) {
            reject({ code: 0, message: e.message });
        }
    });
}

function writeDataFile(jsonfileName, data) {
    return new Promise((resolve, reject) => {
        try {
            fs.truncate(jsonfileName, 0, function() {
                fs.writeFile(jsonfileName, JSON.stringify(data), function(err) {
                    if (err) throw err;
                    resolve(true);
                });
            });
        } catch (e) {
            reject({ code: 0, message: e.message });
        }
    });
}

module.exports.getAtmAvilable = async(payload) => {
    const { cityName = "" } = payload;
    if (cityName) {
        //fetching available city list
        const first_letter = cityName.slice(0, 1).toUpperCase();
        const result = await readDataFile("ingAtm_dataset.json");
        if (result.code) {
            const ingAtm_dataset = result["data"];
            const availableIngATMs =
                ingAtm_dataset[first_letter] && ingAtm_dataset[first_letter].length ?
                ingAtm_dataset[first_letter].filter(
                    (city) =>
                    city["address"]["city"].includes(cityName) &&
                    city.isAvailable == true
                ) : [];
            return {
                code: 1,
                message: `All available atms list in given city i.e. ${cityName}`,
                data: availableIngATMs,
            };
        } else {
            return { code: 0, message: "Data Set not found." };
        }
    } else {
        return {
            code: 0,
            message: "In which city you are looking to create ATM ? Please provide city name in params. ",
        };
    }
};

module.exports.createINGAtm = async(payload) => {
    const { city = "", postalcode = "" } = payload["address"] || payload;
    if (city && postalcode) {
        let ingAtmsData = [];
        const first_letter = city.slice(0, 1).toUpperCase();
        let result = await readDataFile("ingAtm_dataset.json");
        if (result.code) {
            let ingAtm_dataset = result["data"];
            const isAvailable =
                ingAtm_dataset[first_letter] &&
                ingAtm_dataset[first_letter].length &&
                ingAtm_dataset[first_letter].find(
                    (data) =>
                    data["address"]["city"] == city &&
                    data["address"]["postalcode"] == postalcode &&
                    data.isAvailable == true
                );
            if (isAvailable) {
                result = await readDataFile("ingAtm_data.json");
                if (result.code) {
                    ingAtmsData = result["data"];
                    const atmObj = {
                        ...isAvailable,
                        isActive: true,
                        createAt: new Date(),
                        atmID: ingAtmsData.length + 1,
                    };
                    ingAtmsData.push(atmObj);
                    await writeDataFile("ingAtm_data.json", ingAtmsData);
                    let updateDataSet = [];
                    ingAtm_dataset[first_letter].forEach((info) => {
                        if (
                            info["address"]["city"] == city &&
                            info["address"]["postalcode"] == postalcode
                        ) {
                            info.isAvailable = false;
                        }

                        updateDataSet.push(info);
                    });
                    ingAtm_dataset[first_letter] = updateDataSet;

                    await writeDataFile("ingAtm_dataset.json", ingAtm_dataset);
                    return { code: 1, message: "ATM created successfully", data: atmObj };
                }
            } else {
                return {
                    code: 0,
                    message: `ATM location is not available in given city:- ${city} and postalcode:- ${postalcode}.`,
                };
            }
            return true;
        } else {
            return { code: 0, message: "Data Set not found." };
        }
    } else {
        return { code: 0, message: "City name not found." };
    }
};

module.exports.getINGAtm = async(payload) => {
    let { atmID = null } = payload;
    const result = await readDataFile("ingAtm_data.json");
    if (result.code) {
        if (atmID) {
            atmID = parseInt(atmID);
            const _filteredData =
                result["data"] && result["data"].length ?
                result["data"].find(
                    (city) => city["atmID"] === atmID && city.isActive == true
                ) : [];

            return { code: 1, data: _filteredData ? _filteredData : [] };
        } else {
            return {
                code: 1,
                data: result["data"].filter((data) => data.isActive == true),
            };
        }
    }
};

module.exports.deleteINGAtm = async(payload) => {
    let { atmID = null, city = "", postalcode = "" } = payload;
    if (atmID) {
        atmID = parseInt(atmID);
        let result = await readDataFile("ingAtm_data.json");
        if (result.code) {
            let atmData = [];
            result["data"] &&
                result["data"].length &&
                result["data"].forEach((atmObj) => {
                    if (atmObj["atmID"] === atmID) {
                        city = atmObj["address"]["city"];
                        postalcode = atmObj["address"]["postalcode"];
                        atmObj.isActive = false;
                    }

                    atmData.push(atmObj);
                });

            await writeDataFile("ingAtm_data.json", atmData);

            result = await readDataFile("ingAtm_dataset.json");
            if (result.code) {
                let ingAtm_dataset = result["data"];
                let updateDataSet = [];
                const first_letter = city.slice(0, 1).toUpperCase();
                ingAtm_dataset[first_letter].forEach((info) => {
                    if (
                        info["address"]["city"] == city &&
                        info["address"]["postalcode"] == postalcode
                    ) {
                        info.isAvailable = true;
                    }

                    updateDataSet.push(info);
                });
                ingAtm_dataset[first_letter] = updateDataSet;

                await writeDataFile("ingAtm_dataset.json", ingAtm_dataset);
                return { code: 1, message: "Delete Record Successfully." };
            }
        } else {
            return { code: 0, message: "ATMs data not found" };
        }
    } else {
        return { code: 0, message: "ATM ID not found." };
    }
};

module.exports.updateINGAtm = async(payload) => {
    let {
        atmID = null,
            address,
            distance,
            openingHours,
            functionality,
            type,
    } = payload;
    const { street, housenumber, geoLocation } = address;

    const result = await readDataFile("ingAtm_data.json");
    if (result.code) {
        if (atmID) {
            atmID = parseInt(atmID);
            const atmData =
                result["data"] && result["data"].length ?
                result["data"].find(
                    (city) => city["atmID"] === atmID && city.isActive == true
                ) :
                null;

            let updatedObj = {
                address: {
                    street,
                    housenumber,
                    city: atmData["address"]["city"],
                    geoLocation,
                    postalcode: atmData["address"]["postalcode"],
                },
                distance,
                openingHours,
                functionality,
                type,
            };
            updatedObj = {...atmData, ...updatedObj };
            result["data"].forEach((info, i) => {
                if (info.atmID === atmID) {
                    result["data"][i] = updatedObj;
                }

            })

            await writeDataFile("ingAtm_data.json", result["data"]);
            return { code: 1, message: "Record updated successfully", data: updatedObj };
        } else {
            return { code: 0, message: "atmID not found." };
        }
    } else {
        return { code: 0, message: "Data not found." };
    }
};