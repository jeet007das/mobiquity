const { dataValidation } = require("../utils/payloadValidation")
const { user_collection = [] } = require("../datasource/index")
const bcrypt = require("bcrypt-nodejs")

module.exports.createUser = async(payload) => {
    const { name = "", userID = "", password = "" } = payload
    //Here payload validation
    const validate = await dataValidation(payload, ["name", "userID", "password"])
    if (validate['code']) {
        // value is ok, use it
        //Here have to check is there any user present with same mobile number in debugger
        const isUserPresent = user_collection.find(user => user['userID'] === userID)
        if (!isUserPresent) {
            //  const createUser = await User.create({ name, contactNumber, gender, address, country, password })
            let userObj = { name, userID, createdAt: new Date() }

            // becrypt password
            if (password) userObj.password = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);

            // save user
            user_collection.push(userObj);
            return { code: 1, message: "User created successfully", user_collection }
        } else return { code: 0, message: `User already present with userID:- ${userID}` }
    } else {
        const fieldName = validate['info'].toString()
        return { code: 0, message: `This field is required :- ${fieldName}` }
    }

}